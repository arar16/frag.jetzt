export const highcontrast = {

  '--primary': '#fb9a1c',
  '--primary-variant': '#1e1e1e',

  '--secondary': '#fb9a1c',
  '--secondary-variant': '#fb9a1c',

  '--background': '#141414',
  '--surface': '#1e1e1e',
  '--dialog': '#37474f',
  '--cancel': 'Firebrick',
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',

  '--on-primary': '#141414',
  '--on-secondary': '#141414',
  '--on-primary-variant': '#FFFFFF',
  '--on-background': '#FFFFFF',
  '--on-surface': '#FFFFFF',
  '--on-dialog': '#FFFFFF',
  '--on-cancel': '#ffffff',

  '--green': '#3ce933',
  '--red': 'red',
  '--white': '#ffffff',
  '--yellow': '#fb9a1c',
  '--blue': '#3833e9',
  '--purple': '#e933e2',
  '--magenta': '#ea0a8e',
  '--light-green': '#33e98d',
  '--grey': '#7e7e7e',
  '--grey-light': '#9c9c9c',
  '--black': 'black',
  '--moderator': 'black',

  '--questionwall-intro-primary':'yellow',
  '--questionwall-intro-secondary':'#eadabf',
  '--questionwall-intro-background':'#121212'

};

export const highcontrast_meta = {

  translation: {
    name: {
      en: 'Contrast mode',
      de: 'Kontrastmodus',
    },
  },
  isDark: true,
  availableOnMobile: true,
  order: 0,
  scale_desktop: 1,
  scale_mobile: 1,
  previewColor: 'secondary',

};


















