export const arsnova = {

  '--primary': '#073a52',
  '--primary-variant': 'LightGoldenrodYellow',

  '--secondary': 'maroon',
  '--secondary-variant': 'lightgreen',

  '--background': 'Moccasin',
  '--surface': 'LightGoldenrodYellow',
  '--dialog': 'Navajowhite',
  '--cancel': 'Firebrick',
  '--alt-surface': '#eeeeee',
  '--alt-dialog': 'Moccasin',

  '--on-primary': '#fafad2',
  '--on-secondary': '#fafad2',
  '--on-primary-variant': '#000000',
  '--on-background': '#000000',
  '--on-surface': '#000000',
  '--on-dialog': '#000000',
  '--on-cancel': '#ffffff',

  '--green': 'green',
  '--red': 'red',
  '--white': '#ffffff',
  '--yellow': 'red',
  '--blue': '#002878',
  '--purple': '#9c27b0',
  '--magenta': '#ea0a8e',
  '--light-green': 'lightgreen',
  '--grey': 'slategrey',
  '--grey-light': '#EEEEEE',
  '--black': '#000000',
  '--moderator': 'lightsalmon',

  '--questionwall-intro-primary':'yellow',
  '--questionwall-intro-secondary':'#eadabf',
  '--questionwall-intro-background':'#121212'

};

export const arsnova_meta = {

  translation: {
    name: {
      en: 'Light mode',
      de: 'Light Mode'
    },
  },
  isDark: false,
  availableOnMobile: true,
  order: 3,
  scale_desktop: 1,
  scale_mobile: 1,
  previewColor: 'background'

};
