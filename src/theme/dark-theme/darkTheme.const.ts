export const dark = {

  '--primary' : 'darkorange',
  '--primary-variant': 'saddlebrown',

  '--secondary': 'darkgreen',
  '--secondary-variant': '#6f74dd',

  '--background': '#121212',
  '--surface': '#052338',
  '--dialog': '#09394f',
  '--cancel': 'Firebrick',
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',

  '--on-primary': '#000000',
  '--on-secondary': '#eadabf',
  '--on-primary-variant': '#eadabf',
  '--on-background': '#eadabf',
  '--on-surface': '#eadabf',
  '--on-dialog': '#eadabf',
  '--on-cancel': '#000000',

  '--green': 'darkgreen',
  '--red': 'firebrick',
  '--white': '#ffffff',
  '--yellow': 'yellow',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--magenta': '#ea0a8e',
  '--light-green': 'lightgreen',
  '--grey': 'slategrey',
  '--grey-light': '#9E9E9E',
  '--black': 'black',
  '--moderator': 'black',

  '--questionwall-intro-primary':'yellow',
  '--questionwall-intro-secondary':'#eadabf',
  '--questionwall-intro-background':'#121212'

};

export const dark_meta = {

  translation: {
    name: {
      en: 'Dark mode',
      de: 'Dark Mode'
    },
  },
  isDark: true,
  availableOnMobile: true,
  order: 2,
  scale_desktop: 1,
  scale_mobile: 1,
  previewColor: 'background'

};
